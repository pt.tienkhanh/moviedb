package com.buily.moviedb.data.source.local;

import com.buily.moviedb.data.model.Movie;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;

public interface IMovieLocalDataSource {

    Flowable<List<Movie>> getAllMovies();

    Flowable<Movie> getMovie(long id);

    Completable addToFavorite(Movie movie);

    Completable deleteFavorite(Movie movie);
}
