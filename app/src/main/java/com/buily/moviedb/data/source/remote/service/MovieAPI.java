package com.buily.moviedb.data.source.remote.service;

import com.buily.moviedb.data.response.MovieResponse;
import com.buily.moviedb.data.response.YoutubeResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MovieAPI {

    @GET("movie/{type}")
    Observable<MovieResponse> getMovies(@Path("type") String type,
                                        @Query("page") int page);

    @GET("movie/{id}/credits")
    Observable<MovieResponse> getListCasts(@Path("id") Long movieId);

    @GET("movie/{id}/videos")
    Observable<YoutubeResponse> getYoutubeKey(@Path("id") Long movieId);
}
