package com.buily.moviedb.data.source.local.service;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.buily.moviedb.data.model.Movie;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;

@Dao
public interface IMovieDao {

    @Query("select * from movie")
    Flowable<List<Movie>> getAllMovies();

    @Query("select * from movie where id = :id")
    Flowable<Movie> getMovie(long id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Completable addToFavorite(Movie movie);

    @Delete
    Completable deleteFavorite(Movie movie);
}
