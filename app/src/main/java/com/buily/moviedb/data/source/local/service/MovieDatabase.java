package com.buily.moviedb.data.source.local.service;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.buily.moviedb.data.model.Movie;

@Database(entities = Movie.class, version = 2)
public abstract class MovieDatabase extends RoomDatabase {

    public static MovieDatabase instance = null;

    public static MovieDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context,
                    MovieDatabase.class, "favorite_movie.db")
                    .fallbackToDestructiveMigration()
                    .build();
        }

        return instance;
    }

    public abstract IMovieDao movieDao();
}
