package com.buily.moviedb.data.model;

public class ModelF6<T1, T2, T3, T4, T5, T6> {

    private T1 response1;
    private T2 response2;
    private T3 response3;
    private T4 response4;
    private T5 response5;
    private T6 response6;

    public ModelF6(T1 response1, T2 response2, T3 response3, T4 response4, T5 response5, T6 response6) {
        this.response1 = response1;
        this.response2 = response2;
        this.response3 = response3;
        this.response4 = response4;
        this.response5 = response5;
        this.response6 = response6;
    }

    public T1 getResponse1() {
        return response1;
    }

    public void setResponse1(T1 response1) {
        this.response1 = response1;
    }

    public T2 getResponse2() {
        return response2;
    }

    public void setResponse2(T2 response2) {
        this.response2 = response2;
    }

    public T3 getResponse3() {
        return response3;
    }

    public void setResponse3(T3 response3) {
        this.response3 = response3;
    }

    public T4 getResponse4() {
        return response4;
    }

    public void setResponse4(T4 response4) {
        this.response4 = response4;
    }

    public T5 getResponse5() {
        return response5;
    }

    public void setResponse5(T5 response5) {
        this.response5 = response5;
    }

    public T6 getResponse6() {
        return response6;
    }

    public void setResponse6(T6 response6) {
        this.response6 = response6;
    }
}
