package com.buily.moviedb.data.source.repository;

import android.content.Context;

import com.buily.moviedb.data.model.Movie;
import com.buily.moviedb.data.response.MovieResponse;
import com.buily.moviedb.data.response.YoutubeResponse;
import com.buily.moviedb.data.source.local.IMovieLocalDataSource;
import com.buily.moviedb.data.source.local.datasource.MovieLocalLocalDataSource;
import com.buily.moviedb.data.source.remote.IMovieRemoteDataSource;
import com.buily.moviedb.data.source.remote.datasource.MovieRemoteDataSource;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Observable;

public class MovieRepository implements IMovieRemoteDataSource, IMovieLocalDataSource {

    private static MovieRepository sInstance = null;
    private MovieRemoteDataSource mRemoteDataSource;
    private MovieLocalLocalDataSource mLocalDataSource;

    private MovieRepository(MovieRemoteDataSource dataSource, MovieLocalLocalDataSource localDataSource) {
        mRemoteDataSource = dataSource;
        mLocalDataSource = localDataSource;

    }

    public static MovieRepository getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new MovieRepository(MovieRemoteDataSource.getInstance(),
                    MovieLocalLocalDataSource.getInstance(context));
        }

        return sInstance;
    }


    @Override
    public Observable<MovieResponse> getMovies(String type, int page) {
        return mRemoteDataSource.getMovies(type, page);
    }

    @Override
    public Observable<MovieResponse> getListCasts(Long movieId) {
        return mRemoteDataSource.getListCasts(movieId);
    }

    @Override
    public Observable<YoutubeResponse> getYoutubeKey(Long movieId) {
        return mRemoteDataSource.getYoutubeKey(movieId);
    }

    // local

    @Override
    public Flowable<List<Movie>> getAllMovies() {
        return mLocalDataSource.getAllMovies();
    }

    @Override
    public Flowable<Movie> getMovie(long id) {
        return mLocalDataSource.getMovie(id);
    }

    @Override
    public Completable addToFavorite(Movie movie) {
        return mLocalDataSource.addToFavorite(movie);
    }

    @Override
    public Completable deleteFavorite(Movie movie) {
        return mLocalDataSource.deleteFavorite(movie);
    }
}
