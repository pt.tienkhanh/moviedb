package com.buily.moviedb.data.source.remote;

import com.buily.moviedb.data.response.MovieResponse;
import com.buily.moviedb.data.response.YoutubeResponse;

import io.reactivex.Observable;

public interface IMovieRemoteDataSource {

    Observable<MovieResponse> getMovies(String type, int page);

    Observable<MovieResponse> getListCasts(Long movieId);

    Observable<YoutubeResponse> getYoutubeKey(Long movieId);
}
