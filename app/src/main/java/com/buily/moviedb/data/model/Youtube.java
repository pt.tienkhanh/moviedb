package com.buily.moviedb.data.model;

import com.google.gson.annotations.Expose;

public class Youtube {

    @Expose
    private String key;

    @Expose
    private String name;

    @Expose
    private String id;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
