package com.buily.moviedb.data.response;

import com.buily.moviedb.data.model.Youtube;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class YoutubeResponse {

    @Expose
    private Long id;

    @SerializedName("results")
    @Expose
    private List<Youtube> listYoutube;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Youtube> getListYoutube() {
        return listYoutube;
    }

    public void setListYoutube(List<Youtube> listYoutube) {
        this.listYoutube = listYoutube;
    }
}
