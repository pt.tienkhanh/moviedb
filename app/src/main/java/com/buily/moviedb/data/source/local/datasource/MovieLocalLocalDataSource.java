package com.buily.moviedb.data.source.local.datasource;

import android.content.Context;

import com.buily.moviedb.data.model.Movie;
import com.buily.moviedb.data.source.local.IMovieLocalDataSource;
import com.buily.moviedb.data.source.local.service.MovieDatabase;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;

public class MovieLocalLocalDataSource implements IMovieLocalDataSource {

    private static MovieLocalLocalDataSource instance = null;

    private MovieDatabase mDatabase;

    private MovieLocalLocalDataSource(Context context) {
        mDatabase = MovieDatabase.getInstance(context);
    }

    public static MovieLocalLocalDataSource getInstance(Context context) {
        if (instance == null) {
            instance = new MovieLocalLocalDataSource(context);
        }

        return instance;
    }


    @Override
    public Flowable<List<Movie>> getAllMovies() {
        return mDatabase.movieDao().getAllMovies();
    }

    @Override
    public Flowable<Movie> getMovie(long id) {
        return mDatabase.movieDao().getMovie(id);
    }

    @Override
    public Completable addToFavorite(Movie movie) {
        return mDatabase.movieDao().addToFavorite(movie);
    }

    @Override
    public Completable deleteFavorite(Movie movie) {
        return mDatabase.movieDao().deleteFavorite(movie);
    }
}
