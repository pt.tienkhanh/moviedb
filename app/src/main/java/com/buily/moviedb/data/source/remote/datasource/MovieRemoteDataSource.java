package com.buily.moviedb.data.source.remote.datasource;

import com.buily.moviedb.data.response.MovieResponse;
import com.buily.moviedb.data.response.YoutubeResponse;
import com.buily.moviedb.data.source.remote.IMovieRemoteDataSource;
import com.buily.moviedb.data.source.remote.service.MovieAPI;
import com.buily.moviedb.data.source.remote.service.ServiceClient;

import io.reactivex.Observable;

public class MovieRemoteDataSource implements IMovieRemoteDataSource {

    private static MovieRemoteDataSource sInstance = null;
    private MovieAPI mAPI;

    private MovieRemoteDataSource() {
        mAPI = ServiceClient.getInstance();
    }

    public static MovieRemoteDataSource getInstance() {
        if (sInstance == null) {
            sInstance = new MovieRemoteDataSource();
        }

        return sInstance;
    }

    @Override
    public Observable<MovieResponse> getMovies(String type, int page) {
        return mAPI.getMovies(type, page);
    }

    @Override
    public Observable<MovieResponse> getListCasts(Long movieId) {
        return mAPI.getListCasts(movieId);
    }

    @Override
    public Observable<YoutubeResponse> getYoutubeKey(Long movieId) {
        return mAPI.getYoutubeKey(movieId);
    }


}
