package com.buily.moviedb.screen.fav;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.buily.moviedb.base.BaseViewModel;
import com.buily.moviedb.data.model.Movie;
import com.buily.moviedb.data.source.repository.MovieRepository;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class FavoriteViewModel extends BaseViewModel {

    private MovieRepository mRepository;

    private MutableLiveData<List<Movie>> mData;

    public FavoriteViewModel(@NonNull Application application) {
        super(application);
        mRepository = MovieRepository.getInstance(application);

        mData = new MutableLiveData<>();

    }

    @Override
    public void init() {

    }

    public void getAllMovie() {

        mCompositeDisposable.add(mRepository.getAllMovies()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(movies -> mData.setValue(movies), Throwable::printStackTrace));
    }

    public MutableLiveData<List<Movie>> getData() {
        return mData;
    }
}
