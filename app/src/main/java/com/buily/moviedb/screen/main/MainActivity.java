package com.buily.moviedb.screen.main;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.buily.moviedb.R;

/**
 * create by ly 05/01/2019     ^^!
 */

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initFragment();
    }

    private void initFragment() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.frameMain, MainFragment.newInstance())
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }

}
