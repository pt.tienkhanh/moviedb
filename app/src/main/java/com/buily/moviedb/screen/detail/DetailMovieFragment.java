package com.buily.moviedb.screen.detail;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.view.menu.MenuBuilder;
import androidx.appcompat.view.menu.MenuPopupHelper;
import androidx.appcompat.widget.PopupMenu;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;

import com.buily.moviedb.R;
import com.buily.moviedb.base.BaseFragment;
import com.buily.moviedb.base.BaseRecycleViewAdapter;
import com.buily.moviedb.data.model.Cast;
import com.buily.moviedb.data.model.Movie;
import com.buily.moviedb.databinding.FragmentDetailMovieBinding;
import com.buily.moviedb.variable.Constants;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;

import io.reactivex.disposables.CompositeDisposable;

public class DetailMovieFragment extends BaseFragment<FragmentDetailMovieBinding> {

    private static String TAG = DetailMovieFragment.class.getSimpleName();

    private DetailMovieViewModel mDetailMovieViewModel;

    private BaseRecycleViewAdapter<Cast> mCatsAdapter;

    private CompositeDisposable mDisposable;

    private Movie mMovie;
    private boolean isPlaying;

    public static DetailMovieFragment newInstance(Bundle args) {

        DetailMovieFragment fragment = new DetailMovieFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_detail_movie;
    }

    @Override
    protected void initData() {

        mDisposable = new CompositeDisposable();

        Bundle bundle = getArguments();

        mDetailMovieViewModel = new ViewModelProvider(this).get(DetailMovieViewModel.class);
        mFmBinding.setViewModel(mDetailMovieViewModel);

        mDetailMovieViewModel.getMutableListCasts().observe(this, casts -> mCatsAdapter.setData(casts));

        mCatsAdapter = new BaseRecycleViewAdapter<>(mContext, R.layout.item_cast);
        mFmBinding.rcvCasts.setAdapter(mCatsAdapter);

        if (bundle != null) {
            if (bundle.containsKey(Constants.PUT_OBJECT)) {
                mMovie = bundle.getParcelable(Constants.PUT_OBJECT);
                mFmBinding.setData(mMovie);

                assert mMovie != null;
                mDetailMovieViewModel.prepareData(mMovie.getId());
            }
        }

        mFmBinding.imgBack.setOnClickListener(v -> {
            if (isPlaying) {
                for (Fragment fm : mActivity.getSupportFragmentManager().getFragments()) {
                    if (fm instanceof YouTubePlayerSupportFragment) {
                        mActivity.getSupportFragmentManager().beginTransaction()
                                .remove(fm)
                                .commitAllowingStateLoss();
                    }
                }
                isPlaying = false;
                mDetailMovieViewModel.setButtonPLayVisibility(false);
            } else {
                mActivity.onBackPressed();
            }
        });

        mFmBinding.imgPlay.setOnClickListener(v -> initYoutubePlayer());

        mFmBinding.imgMore.setOnClickListener(v -> showPopup(v));

    }

    private void initYoutubePlayer() {
        mDetailMovieViewModel.isPlay.set(true);
        YouTubePlayerSupportFragment playerSupportFragment = YouTubePlayerSupportFragment.newInstance();

        FragmentTransaction transaction = mActivity.getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.youtube, playerSupportFragment);
        transaction.commit();

        playerSupportFragment.initialize(Constants.DEVELOP_KEY, new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                if (!b) {
                    isPlaying = true;

                    youTubePlayer.loadVideo(mDetailMovieViewModel.getYtbKey());
                    youTubePlayer.play();
                }
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
                isPlaying = false;
                Toast.makeText(mContext, youTubeInitializationResult.toString(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    @SuppressLint("RestrictedApi")
    private void showPopup(View view) {

        PopupMenu popupMenu = new PopupMenu(mContext, view);
        popupMenu.inflate(R.menu.item_menu_more_option);

        MenuPopupHelper menuHelper = new MenuPopupHelper(mContext, (MenuBuilder) popupMenu.getMenu(), view);
        menuHelper.setForceShowIcon(true);
        menuHelper.show();

        popupMenu.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.optionFav:

                    mDetailMovieViewModel.addFavoriteMovie(mMovie);

                    return true;
                case R.id.optionRate:
                    //todo: handle rate /** coming soon in ver2 */

                    return true;
            }
            return false;
        });
    }
}
