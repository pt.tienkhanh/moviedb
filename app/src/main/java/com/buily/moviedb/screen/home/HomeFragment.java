package com.buily.moviedb.screen.home;

import android.os.Bundle;
import android.view.View;

import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewpager.widget.ViewPager;

import com.buily.moviedb.R;
import com.buily.moviedb.base.BaseFragment;
import com.buily.moviedb.base.BaseRecycleViewAdapter;
import com.buily.moviedb.base.ItemRecycleViewListener;
import com.buily.moviedb.data.model.Movie;
import com.buily.moviedb.databinding.FragmentHomeBinding;
import com.buily.moviedb.screen.detail.DetailMovieFragment;
import com.buily.moviedb.screen.home.adapter.SlideShowAdapter;
import com.buily.moviedb.screen.seeall.SeeAllFragment;
import com.buily.moviedb.utils.FromType;
import com.buily.moviedb.variable.Constants;

public class HomeFragment extends BaseFragment<FragmentHomeBinding> {

    private SlideShowAdapter mTrendingAdapter;
    private BaseRecycleViewAdapter<Movie> mRecommendAdapter;
    private BaseRecycleViewAdapter<Movie> mPopularAdapter;
    private BaseRecycleViewAdapter<Movie> mTopRateAdapter;
    private BaseRecycleViewAdapter<Movie> mNowPlayingAdapter;
    private BaseRecycleViewAdapter<Movie> mUpcomingAdapter;

    public static HomeFragment newInstance() {

        Bundle args = new Bundle();

        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_home;
    }

    @Override
    protected void initData() {

        initAdapter();

        HomeViewModel homeViewModel = new ViewModelProvider(this).get(HomeViewModel.class);
        mFmBinding.setViewModel(homeViewModel);

        homeViewModel.getMovieResponse().observe(this, movies -> {

            mTrendingAdapter.setData(movies.getResponse1());
            mFmBinding.viewPagerHome.setAdapter(mTrendingAdapter);

            mRecommendAdapter.setData(movies.getResponse2());
            mFmBinding.rcvRecommend.setAdapter(mRecommendAdapter);

            mPopularAdapter.setData(movies.getResponse3());
            mFmBinding.rcvPopular.setAdapter(mPopularAdapter);

            mTopRateAdapter.setData(movies.getResponse4());
            mFmBinding.rcvTopRate.setAdapter(mTopRateAdapter);

            mNowPlayingAdapter.setData(movies.getResponse5());
            mFmBinding.rcvNowPlaying.setAdapter(mNowPlayingAdapter);

            mUpcomingAdapter.setData(movies.getResponse6());
            mFmBinding.rcvUpcoming.setAdapter(mUpcomingAdapter);

        });

        mFmBinding.viewPagerHome.setPageMargin(80);
        mFmBinding.viewPagerHome.setPadding(120, 0, 120, 0);
        mFmBinding.viewPagerHome.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        initSeeAllListener();

    }

    private void initAdapter() {

        mTrendingAdapter = new SlideShowAdapter(mContext);
        mTrendingAdapter.setListener(itemRecycleViewListener);

        mRecommendAdapter = new BaseRecycleViewAdapter<>(mContext, R.layout.item_home_movie);
        mRecommendAdapter.setListener(itemRecycleViewListener);

        mPopularAdapter = new BaseRecycleViewAdapter<>(mContext, R.layout.item_home_movie);
        mPopularAdapter.setListener(itemRecycleViewListener);

        mTopRateAdapter = new BaseRecycleViewAdapter<>(mContext, R.layout.item_home_movie);
        mTopRateAdapter.setListener(itemRecycleViewListener);

        mNowPlayingAdapter = new BaseRecycleViewAdapter<>(mContext, R.layout.item_home_movie);
        mNowPlayingAdapter.setListener(itemRecycleViewListener);

        mUpcomingAdapter = new BaseRecycleViewAdapter<>(mContext, R.layout.item_home_movie);
        mUpcomingAdapter.setListener(itemRecycleViewListener);

    }

    private ItemRecycleViewListener<Movie> itemRecycleViewListener = this::showDetail;

    private void initSeeAllListener() {
        mFmBinding.tvViewMoreRecommend.setOnClickListener(listener);
        mFmBinding.tvViewMorePopular.setOnClickListener(listener);
        mFmBinding.tvViewMoreTopRate.setOnClickListener(listener);
        mFmBinding.tvViewMoreNowPlaying.setOnClickListener(listener);
        mFmBinding.tvViewMoreUpcoming.setOnClickListener(listener);
    }

    private View.OnClickListener listener = v -> {
        switch (v.getId()) {
            case R.id.tvViewMoreRecommend:
                showAllMovie(FromType.TypeMovie.RECOMMEND);
                break;
            case R.id.tvViewMorePopular:
                showAllMovie(FromType.TypeMovie.POPULAR);
                break;
            case R.id.tvViewMoreTopRate:
                showAllMovie(FromType.TypeMovie.TOP_RATE);
                break;
            case R.id.tvViewMoreNowPlaying:
                showAllMovie(FromType.TypeMovie.NOW_PLAYING);
                break;
            case R.id.tvViewMoreUpcoming:
                showAllMovie(FromType.TypeMovie.UPCOMING);
                break;
        }

    };

    private void showDetail(Movie item) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constants.PUT_OBJECT, item);
        FragmentTransaction transaction = mActivity.getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left,
                R.anim.slide_in_left, R.anim.slide_out_right);
        transaction.add(R.id.frameMain, DetailMovieFragment.newInstance(bundle));
        transaction.addToBackStack(null);
        transaction.commitAllowingStateLoss();
    }

    private void showAllMovie(String title) {

        FragmentTransaction transaction = mActivity.getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left,
                R.anim.slide_in_left, R.anim.slide_out_right);
        transaction.add(R.id.frameMain, SeeAllFragment.newInstance(title));
        transaction.addToBackStack(null);
        transaction.commitAllowingStateLoss();
    }

}
