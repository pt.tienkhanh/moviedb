package com.buily.moviedb.screen.main;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.buily.moviedb.R;
import com.buily.moviedb.base.BaseFragment;
import com.buily.moviedb.databinding.FragmentMainBinding;
import com.buily.moviedb.screen.fav.FavoriteFragment;
import com.buily.moviedb.screen.home.HomeFragment;
import com.buily.moviedb.screen.menu.MenuFragment;
import com.buily.moviedb.screen.search.SearchFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainFragment extends BaseFragment<FragmentMainBinding> {

    public static MainFragment newInstance() {

        Bundle args = new Bundle();

        MainFragment fragment = new MainFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_main;
    }

    @Override
    protected void initData() {

        showFragment(HomeFragment.newInstance());

        mFmBinding.bottomNav.setOnNavigationItemSelectedListener(navigationItemSelectedListener);
    }

    private BottomNavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener =
            menuItem -> {
                switch (menuItem.getItemId()) {
                    case R.id.navHome:
                        showFragment(HomeFragment.newInstance());

                        break;
                    case R.id.navFavorite:
                        showFragment(FavoriteFragment.newInstance());

                        break;
                    case R.id.navSearch:
                        showFragment(SearchFragment.newInstance());

                        break;
                    case R.id.navMenu:
                        showFragment(MenuFragment.newInstance());

                        break;
                }

                return true;
            };

    private void showFragment(Fragment fm) {
        FragmentTransaction transaction = mActivity.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frameTab, fm);
        transaction.addToBackStack(null);
        transaction.commitAllowingStateLoss();
    }
}
