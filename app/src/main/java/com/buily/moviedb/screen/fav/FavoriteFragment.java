package com.buily.moviedb.screen.fav;

import android.os.Bundle;

import androidx.lifecycle.ViewModelProvider;

import com.buily.moviedb.R;
import com.buily.moviedb.base.BaseFragment;
import com.buily.moviedb.base.BaseRecycleViewAdapter;
import com.buily.moviedb.data.model.Movie;
import com.buily.moviedb.databinding.FragmentFavoriteBinding;


public class FavoriteFragment extends BaseFragment<FragmentFavoriteBinding> {

    public static FavoriteFragment newInstance() {

        Bundle args = new Bundle();

        FavoriteFragment fragment = new FavoriteFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_favorite;
    }

    @Override
    protected void initData() {

        FavoriteViewModel viewModel = new ViewModelProvider(this).get(FavoriteViewModel.class);
        viewModel.getAllMovie();

        BaseRecycleViewAdapter<Movie> adapter =
                new BaseRecycleViewAdapter<>(mContext, R.layout.item_fav_movie);

        mFmBinding.rcvFav.setAdapter(adapter);

        viewModel.getData().observe(this, adapter::setData);

    }
}
