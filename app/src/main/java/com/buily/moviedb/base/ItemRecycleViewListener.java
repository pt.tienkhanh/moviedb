package com.buily.moviedb.base;

public interface ItemRecycleViewListener<T> {

    void onItemClick(T item);
}
