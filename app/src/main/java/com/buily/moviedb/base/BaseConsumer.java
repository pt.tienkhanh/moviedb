package com.buily.moviedb.base;

import io.reactivex.observers.DisposableObserver;
import retrofit2.HttpException;

public abstract class BaseConsumer <T> extends DisposableObserver<T> {

    @Override
    public void onNext(T t) {
        if(t !=null){
            onSuccess(t);
        }else {
            onError("Error");
        }

    }

    @Override
    public void onError(Throwable e) {
        if(e instanceof HttpException){
            int code =((HttpException) e).code();
            onError(""+code);
        }else {
            onError(e.getMessage());
        }
    }

    @Override
    public void onComplete() {

    }
    protected abstract void onSuccess(T object);
    protected abstract void onError(String error);

}
