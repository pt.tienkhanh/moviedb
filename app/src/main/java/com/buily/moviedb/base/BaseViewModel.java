package com.buily.moviedb.base;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import io.reactivex.disposables.CompositeDisposable;

public abstract class BaseViewModel extends AndroidViewModel {

    protected CompositeDisposable mCompositeDisposable;
    protected Context mContext;

    public BaseViewModel(@NonNull Application application) {
        super(application);

        mContext = application;
        mCompositeDisposable = new CompositeDisposable();

        init();
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        mCompositeDisposable.dispose();
    }

    public abstract void init();
}
